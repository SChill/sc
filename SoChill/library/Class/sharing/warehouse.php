<?php

// Singleton which represents the Main warehouse witch centralizes all the data 
class warehouse_managment{
	
	private static $_instance = null;
	
	public static  function research_file_in_global_warehouse($id){
		$client= new MongoDB\Client('mongodb://localhost:27017');
		$warehouse_manag=$client->So_Chill->Global_Warehouse;
		$result=$warehouse_manag->findOne(['id'=>$id]);
		return $result;
	}
	public static function research_file_in_global_warehouse2($name,$owner){
		$client= new MongoDB\Client('mongodb://localhost:27017');
		$warehouse_manag=$client->So_Chill->Global_Warehouse;
		$result=$warehouse_manag->findOne(['name'=>"$name",'owner'=>"$owner"]);
		return $result;
	}
	
	
	public static  function remove_file_from_warehouse($id){
		echo "$id";
		$result=self::research_file_in_global_warehouse($id);
		$client= new MongoDB\Client('mongodb://localhost:27017');
		$warehouse=$client->So_Chill->Global_Warehouse;
		if($result->number_of_local_warehouse_using_this_file==1){// in the cas where we had only one user ramining, then we can delete it
			$warehouse->deleteOne(['id'=>$id]);	
		}
		else{
			$is_used=($result->number_of_local_warehouse_using_this_file)-1;
			$h=$warehouse->updateOne(['id'=>$id],[ '$set' =>['number_of_local_warehouse_using_this_file' => "$is_used" ]]);
		}
			
	}
	
	
	public static  function add_file_to_warehouse($new_warehouse_file){
		$name=$new_warehouse_file->name;
		$owner=$new_warehouse_file->owner;
		$result=self::research_file_in_global_warehouse2($name,$owner);
		$client= new MongoDB\Client('mongodb://localhost:27017');
		$warehouse=$client->So_Chill->Global_Warehouse;
		if (empty($result))
		{

			$warehouse->insertOne($new_warehouse_file);
			
		}
		else{
			$id=$result->id;
			var_dump($result);
			$is_used=($result->number_of_local_warehouse_using_this_file)+1;
			$h=$warehouse->updateOne(['id'=>$id],[ '$set' =>['number_of_local_warehouse_using_this_file' => $is_used ]]);
		}
		$result=self::research_file_in_global_warehouse2($name,$owner);
		return $result;
	}
	
	
	private function warehouse_managment() {  
	}
	
	
	public static function getInstance() {
		 
		 if(is_null(self::$_instance)) {
		   self::$_instance = new warehouse_managment();  
		 }
	 
		 return self::$_instance;
   }
   
   
}




//An item of the warehouse collection. it's used to fill the  warehouse n local warehouse collections. He's a son of the main item class
class warehouse_file extends item{
	public $file;
	public $number_of_local_warehouse_using_this_file;//used for the case if this object is used by at least 1 local warehouse. If 0, then deleted from collection to avoid useless file in memory	
	public function warehouse_file($name,$type,$description,$owner,$users,$is_private,$file){ 
		parent::item($name,$type,$description,$owner,$users,$is_private);
		$this->file=(string) $file;
		$this->number_of_local_warehouse_using_this_file=1;
		
   }
   public function bsonUnserialize( array $map )
	{
		foreach ( $map as $k => $value )
		{
			$this->$k = $value;
		}
		$this->unserialized = true;
	}
   
}


// This is a container of item from the warehouse . It Belongs to one owner to and can be pirvate or public for the users.
class categorie extends item{
	
	public $files;
	public function categorie($name,$type,$description,$owner,$users,$is_private){ 
		parent::item($name,$type,$description,$owner,$users,$is_private);
		$this->files=array();
		
		
   }
   
   
    public function i_want_that_file_more($file){
		$result=warehouse_managment::add_file_to_warehouse($file);
		$id=$result->id;
		$name=$file->name;
		$owner=$file->owner;
		$arr=array($id,$name,$owner);
		$this->files["$id"]=$arr;
		
	}
	
   public function i_want_to_remove_that_file($name,$user)
   {
	   $bool=0;
		foreach($this->files as $value) {
			if ($value[1]==$name and $user==$value[2])//if this is the right owner for the file named "name"
			{
				$bool=1;
				warehouse_managment::remove_file_from_warehouse($value[0]);
				unset($value);
			}
		}
		if($bool==0){
			echo "Sorry buddy , you're not the owner of this file or we can't find it... So you can't delete it... Chill"; 
		}
	}
	
}




// The local warehouse is the cloud for a determined chill. It's the file proposed for one and unic chill.
class local_warehouse
{
	public $local_categories;
	
	public function local_warehouse(){ 
		$this->local_categories=array();
   }
	
	public function add_categorie_to_local_warehouse($new_warehouse_categorie){
		$name=$new_warehouse_categorie->name;
		if(empty($this->local_categories["$name"])){
			$this->local_categories["$name"]=$new_warehouse_categorie;
		}
		else
		{
			echo "Sorry buddy but this categorie name still exists, try another name... Chill";
		}
	}
	
	public function remove_categorie_from_local_warehouse($name,$user){
		$bool=0;
		foreach($this->local_categories as $categorie) {
			if ($categorie["name"]==$name and $categorie["owner"]==$user)//if this is the right owner for the categorie named "name"
			{
				$bool=1;
				foreach( $categorie["files"] as $value){ //array bc when it's added in mongoDB then it becomes array when outside
					warehouse_managment::remove_file_from_warehouse($value[0]);
				}
				unset($categorie);
			}	
		}
		if($bool==0){
			echo "Sorry buddy , you're not the owner of this categorie or we can't find it... So you can't delete it... Chill"; 
		}
		
	}
}

/*$file=new warehouse_file("hh","gg","test","adri",array("kiki","adri"),false,"docvrvrgvr");
$file2=new warehouse_file("hh","gg","test","adri",array("kiki","adri"),false,"docvrvrgvr");
$file3=new warehouse_file("hh","gg","test","adr",array("kiki","adri"),false,"docvrvrgvr");
$categorie=new categorie("lov","picole","lhkjljj!mm","adri",array("kiki","adri"),false);
$categorie->i_want_that_file_more($file);
$categorie->i_want_that_file_more($file2);
$categorie->i_want_that_file_more($file3);

$categorie->i_want_to_remove_that_file("hh","adrie");
$categorie->i_want_to_remove_that_file("hh","adr");

//$w =new warehouse_file("cours sdd","cours"," ce fichier contient des cours de sdd","ALL",true);
//var_dump($w);
*/
?>

