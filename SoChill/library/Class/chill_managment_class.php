<?php
require '../vendor/autoload.php';
require "item_class.php";
include("sharing/warehouse.php");
require "event/stuff_class.php";
require "event/fridge_class.php";
require "event/event_class.php";
include ("benefit/benefit_class.php");


class chill{
	public $id;
	public $name;
	public $location;
	public $events;
	public $sharing;
	public $showroom;
	public $benefit;
	public static $id_sta=0.0;
	public $number_of_user_using_this_chill;
	public static $nb_of_message=0;
	public $message_list;
	
	//Instanciate New Chill
	public function chill($name,$location){ 
		$this->name=$name;
		$this->location=$location;
		$this->events=array();
		$this->sharing=new local_warehouse();
		$this->showroom=new local_warehouse();
		$this->benefit=new benefit("The chill to be");
		self::$id_sta++;
		$this->id=self::$id_sta;
		$this->number_of_user_using_this_chill=1;
	}
	
	
	
	//It allows to convert a chill from a bson object to a PHP object 
	public function bsonUnserialize( $map )
	{
		
		foreach ( $map as $k => $value )
		{
			$this->$k = $value;
		}
	}
}



// chill manager
class chill_managment{
	
	private static $_instance = null;
	
	//research a chill with the id
	public static  function research_chill_storage($id){
		$client= new MongoDB\Client('mongodb://localhost:27017');
		$chill_manag=$client->So_Chill->Chill_Storage;
		$result=$chill_manag->findOne(['id'=>$id]);
		return $result;
	}
	
	//research a chill with the name and the location
	public static function research_chill_storage2($name,$location){
		$client= new MongoDB\Client('mongodb://localhost:27017');
		$chill_manag=$client->So_Chill->Chill_Storage;
		$result=$chill_manag->findOne(['name'=>"$name",'location'=>"$location"]);
		return $result;
	}
	
	// remove a chill with the name and adress
	public static  function remove_chill_from_storage($nom,$adress){
		$result=self::research_chill_storage2($nom,$adress);
		$client= new MongoDB\Client('mongodb://localhost:27017');
		$chill=$client->So_Chill->Chill_Storage;
		if($result->number_of_user_using_this_chill==1){// in the cas where we had only one user ramining, then we can delete it
			$r=$chill->deleteOne(['location'=>"$adress"]);	
			var_dump($r);
		}
		else{
			$is_used=($result->number_of_user_using_this_file)-1;
			echo"$adress";
			$h=$chill->updateOne(['location'=>"$adress"],[ '$set' =>['number_of_user_using_this_chill' => "$is_used" ]]);
		}
			
	}
	// update a chill event
	public static function update_events($chill)
	{
		$client= new MongoDB\Client('mongodb://localhost:27017');
		$chill_col=$client->So_Chill->Chill_Storage;
		$events=$chill->events;
		$name=$chill->name;
		$location=$chill->location;
		$h=$chill_col->updateOne(
			[ 'name' => "$name",'location'=>"$location" ],
			[ '$set' => [ 'events' => $events]]);
	}
	//update the chat of the chill
	public static function update_message($chill)
	{
		$client= new MongoDB\Client('mongodb://localhost:27017');
		$chill_col=$client->So_Chill->Chill_Storage;
		$message_list=$chill->message_list;
		$name=$chill->name;
		$location=$chill->location;
		$h=$chill_col->updateOne(
			[ 'name' => "$name",'location'=>"$location" ],
			[ '$set' => [ 'message_list' => $message_list]]);
		return $h;
	}
	// add a new chill
	public static function add_chill_to_storage($new_chill){
		$name=$new_chill->name;
		$location=$new_chill->location;
		$result=self::research_chill_storage2($name,$location);
		$client= new MongoDB\Client('mongodb://localhost:27017');
		$chill=$client->So_Chill->Chill_Storage;
		if (empty($result))
		{

			$chill->insertOne($new_chill);
			
		}
		else{
			$adress=$result->location;
			$is_used=($result->number_of_user_using_this_chill)+1;
			var_dump($result);
			$chill->updateOne(['location'=>"$adress"],[ '$set' =>['number_of_user_using_this_chill' => "$is_used" ]]);
			
		}
		return $result;
	}
	
	//instanciate the single
	private function chill_managment() {  
	}
	
	// return the instance if created. If not, it creats it and then return the new instance
	public static function getInstance() {
		 
		 if(is_null(self::$_instance)) {
		   self::$_instance = new chill_managment();  
		 }
	 
		 return self::$_instance;
   }
   
   
}

/*$chill_n= chill_managment::research_chill_storage2("bde-ISIMA","146 rue de la chebarde 63000 clermont");
$list=array("titou");
$chill=new chill("bde-ISIMA","146 rue de la chebarde 63000 clermont");
$chill->bsonUnserialize($chill_n);
$chill->add_new_event("booddd","�a va chier","mail.com","hsh.fr","10/04/2016",$list);
var_dump($chill);
chill_managment::update_events($chill);*/




?>
