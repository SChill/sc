<?php

class item{
	public $name;
	public $type;
	public $description;
	public $owner;
	public $users;
	public $is_private;// if private, the user's name who's the owner, else ALL;
	private static $id_sta=0.0;
	public $id;
	public function item($name,$type,$description,$owner,$users,$is_private){ 
		$this->name=(string) $name;
		$this->type=(string) $type;
		$this->description=(string) $description;
		$this->owner=(string) $owner;
		$this->users=(array)$users;
		$this->is_private=(boolean) $is_private;
		self::$id_sta++;
		$this->id=self::$id_sta;	
   }
}
	
class message_item{
	private static $id_sta=0.0;
	public $id;
	public $message_content;
	public $file_content;
	public $owner; 
	public $date;
	public $place_in_the_chill;
	
	
	public function message($message_content,$user,$place_in_the_chill){
		$this->message_content=$message_content;
		self::$id_sta++;
		$this->id=self::$id_sta;
		$this->owner=$user;
		$this->date=date('Y-m-d H:i:s');
		$this->place_in_the_chill=$place_in_the_chill;
	}
	
}
	

?>
