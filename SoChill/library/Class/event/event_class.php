<?php	

class event{
	public $name;
	public $aim;
	public $stuff;
	public $owner_mail;
	public $fridge;
	public $list_of_music;
	
	
	public function event($name,$description,$owner_mail,$links,$date,$list_of_music)
	{
		$this->name=$name;
		$this->aim=new aim($description,$links,$date);
		$this->links=$links;
		$this->date=$date;
		$this->owner_mail=$owner_mail;
		$this->stuff=new stuff();
		$this->fridge=new fridge();
		$this->list_of_music=$list_of_music;
	}
	
}

class aim{
	public $description;
	public $links;
	public $date;
	
	public function aim($description,$links,$date)
	{
		$this->description=$description;
		$this->links=$links;
		$this->date=$date;
	}
}


?>
