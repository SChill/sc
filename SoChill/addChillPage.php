<!DOCTYPE html>
<html>
  <head>
    <title>Add Chill-SoChill</title>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/login.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Mr+Dafoe|Rokkitt:400,500" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="js/jquery.js"></script> <!-- mettre le lien google wallah -->
    <script src="js/nosFonctions.js"></script>
    <?php
      $SESSION["page"]="add";
    ?>
  </head>
  <body style="overflow:auto;">
    <div class="container" >
    <div class="row">
      <nav id="menu" class="col-sm-2">
        <div class="row">
          <div id="tetePage" class="col-sm-4">
           <a href="./brick_panel/event/playlist.php"><img id="logo" src="Image/frog_-_logo.png" alt="logo" height="140px" width="140px"/></a>
          </div>
          
        </div>
        <div class="menuCat" class="row">
          <div class="cat" class="col-lg-offset-2 col-sm-10">
            <div class="row" style="Line-Height: 40px;">
              <div id="Type">
                <strong>Add a new<br> chill! </strong>
              </div>
            </div>
          </div>
        </div>
      </nav>
      <section id="corpsPage" class="col-sm-8">
        <div id="titrePage">
        <div  class="row"  >
            
            <article class="col-sm-12" >
              <h2 >So Chill</h2>
            </article>
            
            
          </div>
          
        </div>
        <div id="conteneurCat" class="row">
          <article class="catSec" class="col-sm-3">
             <strong style="font-size:60px;">New Chill</strong>
            <table id="signinTable" style="border-spacing: 0px 60px;">
            <form action="library/addChill.php" method="post">
              <tr>
                <td><strong>Name : </strong></td><td><input type="text" name="name" ></td>
              </tr>
              <tr>
               <td><strong>Adress :</strong></td><td> <input type="text" name="adress"> </td>
              </tr>
              <tr>
               <td><strong>City:</strong></td><td> <input type="text" name="city"> </td>
              </tr>
              <tr>
               <td><strong>Country:</strong></td><td> <input type="text" name="country"> </td>
              </tr>
              <tr><td></td><td><input type="submit" value="ADD"></td></tr>
            </form>
          </table> 
          </article>
        </div>
      </section>
    </div>
  </body>
</html>