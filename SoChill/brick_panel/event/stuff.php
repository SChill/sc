<?php
    session_cache_limiter('private_no_expire, must-revalidate');
    session_start();
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Stuff - SoChill</title>
    <link href="../../css/bootstrap.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Mr+Dafoe|Rokkitt:400,500" rel="stylesheet">
    <link href="../../css/common.css" rel="stylesheet">
    <link href="../../css/threeDiv.css" rel="stylesheet">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <script src="../../js/jquery.js"></script>
     <script type="text/javascript">
    
    var varia =0;

  function test(){
    var texte, doc;
    //texte="coucou";
    doc =document.getElementById("SelectChill").value;
    //alert(texte);
    texte=doc;
    alert(texte);
  }

  function link(lien){
    if(lien==1){
      window.location = "notif.html";
    }
    else if(lien==2){
       window.location = "message.html";
    }
    else if(lien==3){
       window.location = "parametre.html";
    }
  }


  function test2(){
    
    
    if(varia==0){
      $('#contenuSupp').append('<div onclick="link(1)" class="tetePageBisHerite">Notif</div><div onclick="link(2)" class="tetePageBisHerite">MSG</div><div onclick="link(3)" class="tetePageBisHerite">Parameter</div>');
      varia++;
    }
    else{
      $('.tetePageBisHerite').remove();
      varia--;
    }


  }
  
</script>
<?php
    require "../../library/Class/chill_managment_class.php";
     $chill_n=chill_managment::research_chill_storage2($_SESSION["chill_name"],$_SESSION["chill_place"]);
    $chill=new chill($chill_n["name"],$chill_n["location"]);
    $chill->bsonUnserialize($chill_n);
    $ename=$_SESSION["event_name"];
    foreach ($chill->events as $key => $value) {
      if ($chill->events[$key]->name==$_SESSION["event_name"]){
        $event=$chill->events[$key];
        $stuff=$chill->events[$key]->stuff;
      }
    }               
  ?>
  <script language="javascript" type="text/javascript">
        var i=0;
        function refreshTchat(){
        var chill_place= '<?php echo $_SESSION["chill_place"];?>';
        var chill_name='<?php echo $_SESSION["chill_name"];?>';
        $.ajax({ type: "GET", url: "../../ajax/refresh.php", data: "action=refresh&chill_place="+chill_place+"&chill_name="+chill_name, success: function(msg){ document.getElementById("divMessage").innerHTML = msg; } });
          if(i==0){
          i++;
        }
        else{
          if(i==1)
          {
            $('#divMessage').scrollTop($('#divMessage')[0].scrollHeight);
            i++;
          }
         
      }
        setTimeout("refreshTchat()",400); 
        }



        function envoi(){
          
        

        var message = document.getElementById("boum").value;

        var pseudo = '<?php echo $_SESSION["mail"];?>';
        var chill_place= '<?php echo $_SESSION["chill_place"];?>';
        var chill_name='<?php echo $_SESSION["chill_name"];?>';

        $.ajax({ type: "GET", url: "../../ajax/refresh.php", data: "action=envoi&message="+message+"&pseudo="+pseudo+"&chill_place="+chill_place+"&chill_name="+chill_name, success: function(msg){ document.getElementById("etat").innerHTML = msg;} });

        }

        function refresh2(){
          $('#divMessage').scrollTop($('#divMessage')[0].scrollHeight);
        }

        refreshTchat();
</script>
  </head>
  <body style="overflow:auto;">
	
    <div class="container" >
      

      <div class="row">
        



        <nav id="menu" class="col-sm-2">
          <div class="row">
            <div id="divLogo" id="tetePage" class="col-sm-12">
            <a href="playlist.php" target="_blank" ><img id="logo" src="../../Image/frog_-_logo.png" alt="logo" height="140px" width="140px"/></a>
            </div>
          </div>
          <div class="menuCat" class="row">
            <div class="cat" class="col-lg-offset-2 col-sm-10">
              <div class="row">
                <div class="Type">
                  <a href="../../index.php">Chill List</a>
               </div>
              </div>
            </div>
          </div>
          <div class="menuCat" class="row">
            <div class="cat" class="col-lg-offset-2 col-sm-10">
              <div class="row">
                <div class="Type">
                  <a href="../Chill.php"><?php echo $chill->name;?></a>
               </div>
              </div>
            </div>
          </div>
          <div class="menuCat" class="row">
            <div class="cat" class="col-lg-offset-2 col-sm-10">
              <div class="row">
                <div class="Type">
                   <a href="../event.php"><?php echo $event->name;?></a>
                </div>
              </div>
            </div>
          </div>
          <div class="menuCat" class="row">
            <div class="cat" class="col-lg-offset-2 col-sm-10">
              <div class="row">
                <div class="Type">
                   <a href="fridge.php">Fridge</a>
                </div>
              </div>
            </div>
          </div>
        </nav>


        <section id="corpsPage" class="col-sm-8" style="overflow:auto;">
           <div id="titrePage">
        <div  class="row"  >
            <article class="col-sm-4" id="formulaire">
              <form action="../../researchChill.php" method="post">
                <input class="champ" type="text" onfocus="if(this.value == this.defaultValue) this.value = ''" name="search" value="Search Chill..." id="researchBar" style="font-family: 'Rokkitt', serif;"/>
                    <input class="bouton" type="submit" value="search" style="font-family: 'Rokkitt', serif;" />
              </form>
            </article>
            <article class="col-sm-4" >
              <h2 ><?php echo $ename;?>'s Chill Stuff</h2>
            </article>
            </article>
            
            <article class="col-sm-1" style="font-size:20px;Line-Height: 30px; margin-top:3%;">Unconnect
              <a href="../../library/unco.php">
                <span class="glyphicon glyphicon-off"></span>
              </a></article></article>
          </div>
          
        </div>
          <div id="conteneurCat" class="row">
            <article class="catSec" class="col-sm-3" style="overflow:auto;">
              <strong style="font-size:60px;">Music</strong>
              <a href="addMusicPage.php" class="btn btn-info btn-lg">
                  <span class="glyphicon glyphicon-plus"></span> Bring Music Equip
                </a>
              <?php
              foreach ($stuff->music_stuff_table as $music_stuff) {
                echo"<div style='Line-Height:25px;'>Name:".$music_stuff->name."<br>Type:".$music_stuff->type."<br>Description:".$music_stuff->description."<br>Owner:".$music_stuff->owner."</div><br>";

              }
              ?>
              
            </article>
            <article class="catSec" class="col-sm-3" style="overflow:auto;">
              <strong style="font-size:60px;">Furnitures</strong>
              <a href="addFurniturePage.php" class="btn btn-info btn-lg">
                  <span class="glyphicon glyphicon-plus"></span> Bring Furniture
                </a>
              <?php
              foreach ($stuff->furniture_table as $furniture_stuff) {
                  echo"<div style='Line-Height:25px;'>Name:".$furniture_stuff->name."<br>Owner:".$furniture_stuff->owner."</div><br>";

              }
              ?>
              
            </article>
            <article class="catSec" class="col-sm-3" style="overflow:auto;">
              <strong style="font-size:60px;">Other</strong>
              <a href="addOtherPage.php" class="btn btn-info btn-lg">
                  <span class="glyphicon glyphicon-plus"></span> Bring Other Kind of Stuff
                </a>
              <?php
              foreach ($stuff->other as $other_stuff) {
                echo"<div style='Line-Height:25px;'>Name:".$other_stuff->name."<br>Description:".$other_stuff->description."<br>Owner:".$other_stuff->owner."</div><br>";
              }
              ?>
              
            </article>
            
          </div>
        </section>


         <nav id="message" style="font-family: 'Rokkitt', serif;" class="col-sm-2">
          <div class="row">

          </div>
          <div id="msgSec" class="row">
             <h1 id="titreMsg"><strong>Chill Trend</strong></h1>
            <div class="col-sm-12" id="plancheMessage">
              <form>
                <div id="SelectChill" name="nom" style="font-family: 'Mr Dafoe', cursive; font-size:30px;">
                  HEY! <br><div id="etat"></div>
                </div>
              </form>
                <div id="divMessage" style="border-style: double; border-radius:15px;overflow:auto;">
                  </div>
                <div id="inputMessage" style='margin-top:8%;' >
                  <input style="width:70%; height:30px;" onfocus="if(this.value == this.defaultValue) this.value = ''" onkeydown = "if (event.keyCode == 13)
                        document.getElementById('envoie').click()"  type="text" id ="boum" value="message ecrit"/>
                  <input id="envoie" type="submit" value ="Send" onclick="envoi(); setTimeout('refresh2()',499);"  style='margin-top:2%; width:30%; '>
                </div>
            </div>
          </div>
        </nav>

      
    </div>
  </body>
</html>
