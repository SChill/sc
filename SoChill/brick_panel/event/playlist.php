<?php
    session_cache_limiter('private_no_expire, must-revalidate');
    session_start();
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Chill Out - SoChill</title>
    <link href="../../css/bootstrap.css" rel="stylesheet">
    <link href="../../css/common.css" rel="stylesheet">
    <link href="../../css/oneDiv.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Mr+Dafoe|Rokkitt:400,500|Lora:400,700" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="../../js/jquery.js"></script> <!-- mettre le lien google wallah -->
    <script src="../../js/nosFonctions.js"></script>
    <script src="http://www.youtube.com/player_api"></script>
    <script type="text/javascript">

      var player;
      var compteur=0;
        var tab=["waka", "psg barca higlights","lalaland"];

      function go_get(){
         var base_url = 'http://www.youtube.com/embed?listType=search&list=';
         var search_field = document.getElementById('yourtextfield').value;
         var target_url = base_url + search_field;
         var ifr = document.getElementById('youriframe');
         ifr.src = target_url;
         return false;
      }

      
        
    </script>
    <?php
    require "../../library/Class/chill_managment_class.php";
    $chill_n=chill_managment::research_chill_storage2($_SESSION["chill_name"],$_SESSION["chill_place"]);
    $chill=new chill($chill_n["name"],$chill_n["location"]);
    $chill->bsonUnserialize($chill_n);              
  ?>
  </head>
  <body style="overflow:auto;background-image:url(../../Image/music.jpg);background-size: auto;">
    <div class="container"  >
    <div class="row">
      <nav id="menu" class="col-sm-2">
        <div id="divLogo" class="row">
          <div id="tetePage" class="col-sm-4">
            <img id="logo" src="../../Image/frog_-_logo.png" alt="logo" height="140px" width="140px"/>
          </div>
          
        </div>
        <div class="menuCat" class="row">
          <div class="cat" class="col-lg-offset-2 col-sm-10">
            <div class="row" style="overflow:auto;Line-Height: 40px;">
              <div id="Type">
                <strong style="margin-left:4%;">Just a paradise<br>
                  to relax your<br> hears</strong>
              </div>
            </div>
          </div>
        </div>
        <div class="menuCat" class="row">
            <div class="cat" class="col-lg-offset-2 col-sm-10">
              <div class="row">
                <div class="Type">
                  <a href="../../index.php">Chill List</a>
               </div>
              </div>
            </div>
          </div>
      </nav>
      <section id="corpsPage" class="col-sm-8">
        <div id="titrePage">
        <div  class="row"  >
            <article class="col-sm-4" id="formulaire">
              <form action="../../researchChill.php" method="post">
                <input class="champ" type="text" onfocus="if(this.value == this.defaultValue) this.value = ''" name="search" value="Search Chill..." id="researchBar" style="font-family: 'Rokkitt', serif;"/>
                    <input class="bouton"  type="submit" value="search" style="font-family: 'Rokkitt', serif;" />
              </form>
            </article>
              <article class="col-sm-4" >
                <h2 ><?php
                $name=$_SESSION['name_of_user'];
                echo"$name";
                      ?>'s Liberty</h2>
              </article>
            </article>
            <article class="col-sm-1" style="font-size:20px;Line-Height: 30px; margin-top:3%;">
              Unconnect
              <a href="../../library/unco.php">
                <span class="glyphicon glyphicon-off"></span>
              </a></article>
           
           </div>
          
        </div>
        <div id="conteneurCat" class="row" >
          <article class="catSec" class="col-sm-3" style="overflow:auto; height:100%;line-height:60px;">
              <strong style="font-size:60px;">Enjoy Your Freedom</strong>
              <form onsubmit="go_get(); return false;" >
                  <input type="value" onfocus="if(this.value == this.defaultValue) this.value = ''"  value="Artist/Song..." style="height:30px; width:300px;"  id="yourtextfield"/>
                  <input type="submit" class="btn btn-info btn-bg" value="Search Artist/Song" />
              </form>
           
                <iframe id="youriframe" width="100%" height="450" style="border-radius:5%;border-style:none;" allowfullscreen autoplay="1" ></iframe>
            
          </article>
        </div>
      </section>

    
    </div>
  </body>
</html>
