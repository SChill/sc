<?php
  session_cache_limiter('private_no_expire, must-revalidate');
  session_start();
?>
<!DOCTYPE html>
<html>
  <head>
    <title>SoChill</title>
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../css/login.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Mr+Dafoe|Rokkitt:400,500" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="js/jquery.js"></script> <!-- mettre le lien google wallah -->
    <script src="js/nosFonctions.js"></script>
  </head>
  <body style="overflow:auto;">
    <div class="container" >
    <div class="row">
      <nav id="menu" class="col-sm-2">
        <div class="row">
          <div id="tetePage" class="col-sm-4">
           <a href="../index.php"><img id="logo" src="../Image/frog_-_logo.png" alt="logo" height="140px" width="140px"/></a>
          </div>
          
        </div>
        <div class="menuCat" class="row">
          <div class="cat" class="col-lg-offset-2 col-sm-10">
            <div class="row">
              <div class="row" style="Line-Height: 40px;">
              <div id="Type">
                <strong>Here you can<br> change the aim <br>
                of your own <br> event </strong>
              </div>
            </div>
            </div>
          </div>
        </div>
      </nav>
      <section id="corpsPage" class="col-sm-8">
        <div id="titrePage">
        <div  class="row"  >
            
            <article class="col-sm-12" >
              <h2 >So Chill</h2>
            </article>
            
            
          </div>
          
        </div>
        <div id="conteneurCat" class="row" >
          <article class="catSec" class="col-sm-3">
             <strong style="font-size:60px;height:80%;">New Event</strong>
            <table id="signinTable" style="border-spacing: 0px 60px;overflow:auto;">
            <form action="../library/updateEvent.php" method="post">
               <td><strong>Description :</strong></td><td> <?php echo'<input type="text" name="description" value='.$_SESSION["Description"].'>';?> </td>
              </tr>
              <tr>
               <td><strong>Link Facebook :</strong></td><td> <?php echo'<input type="text" name="links" value='.$_SESSION["Links"].'>';?> </td>
              </tr>
              <tr>
               <td><strong>Date :</strong></td><td> <?php echo'<input type="text" name="date" value='.$_SESSION["Date"].'>';?> </td>
              </tr>
              <tr><td></td><td><input type="submit" value="ADD"></td></tr>
            </form>
          </table> 
          </article>
        </div>
      </section>
    </div>
  </body>
</html>

