<?php
    session_cache_limiter('private_no_expire, must-revalidate');
    session_start();
?>
<!DOCTYPE html>
<html>
  <head>
    <title>The Wall - SoChill</title>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/common.css" rel="stylesheet">
    <link href="css/oneDiv.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Mr+Dafoe|Rokkitt:400,500|Lora:400,700" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="js/jquery.js"></script>
    <script src="js/nosFonctions.js"></script>
    <?php
    require "library/Class/user_managment_class.php";
      $mail=$_SESSION['mail'];
      $user=user_managment::research_user2($mail);
    ?>
  </head>
  <body style="overflow:auto;background-image:url(Image/joli.jpg)">
    <div class="container"  >
    <div class="row">
      <nav id="menu" class="col-sm-2">
        <div id="divLogo" class="row">
          <div id="tetePage" class="col-sm-4">
            <a href="./brick_panel/event/playlist.php" target="_blank" ><img id="logo" src="Image/frog_-_logo.png" alt="logo" height="140px" width="140px"/></a>
          </div>
          
        </div>
        <div class="menuCat" class="row">
          <div class="cat" class="col-lg-offset-2 col-sm-10">
            <div class="row" style="overflow:auto;Line-Height: 40px;">
              <div id="Type">
                <strong style="margin-left:4%;"><?php
                $message=$user->name;
                echo "Hey ".$message."!<br> What's up guy?"; 
                ?></strong>
              </div>
            </div>
          </div>
        </div>
        <div class="menuCat" class="row">
            <div class="cat" class="col-lg-offset-2 col-sm-10">
              <div class="row" style="overflow:auto;Line-Height: 40px;">
                
                  <strong>The wall <br>contains<br> chill you <br>follow.<br> Let's add one<br>to see what<br> it is!</strong>
               
              </div>
              
            </div>
          </div>
      </nav>
      <section id="corpsPage" class="col-sm-8">
        <div id="titrePage">
        <div  class="row"  >
            <article class="col-sm-4" id="formulaire">
              <form action="researchChill.php" method="post">
                <input class="champ" type="text" onfocus="if(this.value == this.defaultValue) this.value = ''" name="search" value="Search Chill..." id="researchBar" style="font-family: 'Rokkitt', serif;"/>
                    <input class="bouton" type="submit" value="search" style="font-family: 'Rokkitt', serif;" />
              </form>
            </article>
              <article class="col-sm-4" >
                <h2 ><?php
                $name=$_SESSION['name_of_user'];
                echo"$name";
                      ?>'s Chill List</h2>
              </article>
            </article>
           
            <article class="col-sm-1" style="font-size:20px;Line-Height: 30px; margin-top:3%;">
              Unconnect
              <a href="library/unco.php">
                <span class="glyphicon glyphicon-off"></span>
              </a></article>
           </div>
          
        </div>
        <div id="conteneurCat" class="row" >
          <article class="catSec" class="col-sm-3" style="overflow:auto;">
            <strong style="font-size:60px;">The Wall</strong><br>
            <a href="addChillPage.php" class="btn btn-info btn-lg">
                  <span class="glyphicon glyphicon-plus"></span> Add Chill 
              </a>
              <br>
            <?php   
                $user_mail=$_SESSION["mail"];//$_SESSION["user_mail"];
                $user=user_managment::research_user2($user_mail);
                $user_chill=$user["list_of_chill"];
                while (list($location, $chill) = each($user_chill)) {

                  $e=preg_split("[\n]", $chill);
                  $ebis=preg_split("[,]", $e[1]);
                  
                  $style="font-family: 'Lora', serif";
                  echo'
                        
                        <form action="Chill.php" method="post" style="Line-Height: 40px">
                          <input type="hidden" name="chill_name" value="'.$e[0].'">
                          <input type="hidden" name="chill_place" value="'.$e[1].'">
                          <input type="submit" value="Name:'.$e[0].' 
Adress:
'.$ebis[0].'
'.$ebis[1].'" class="chillList" style="background-color:rgba(242, 200, 150, 0.5);font-size:20px; Line-Height:30px;border-radius:20px; border-width:1px; margin-left:6px; border-radius:6px;text-overflow: ellipsis;font-size: 20px;padding: 6px 0 2px 8px;'.$style.';">
                        </form>';

                }
            ?>
          </article>
        </div>
      </section>
    </div>
  </body>
</html>
