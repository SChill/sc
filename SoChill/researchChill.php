<?php
    session_cache_limiter('private_no_expire, must-revalidate');
    session_start();
?>
<!DOCTYPE html>
<html>
  <head>
    <title>SoChill</title>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/login.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Mr+Dafoe|Rokkitt:400,500" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="js/jquery.js"></script> <!-- mettre le lien google wallah -->
    <script src="js/nosFonctions.js"></script>
    <?php
        require "./library/Class/user_managment_class.php";
        
        $search=htmlspecialchars($_POST['search']);
        $Client= new MongoDB\Client('mongodb://localhost:27017');
        $regex = new MongoDB\BSON\Regex ("^".$search."");
        $chill_cursor=$Client->So_Chill->Chill_Storage->find(array('name' => $regex));
        $_SESSION['page']="research";
        $mail=$_SESSION['mail'];
        $user=user_managment::research_user2($mail);
    ?>
  </head>
  <body style="overflow:auto;">
    <div class="container"  >
    <div class="row">
      <nav id="menu" class="col-sm-2">
        <div id="divLogo" class="row">
          <div id="tetePage" class="col-sm-4">
            <a href="./brick_panel/event/playlist.php" target="_blank" ><img id="logo" src="Image/frog_-_logo.png" alt="logo" height="140px" width="140px"/></a>
          </div>
          
        </div>
      </nav>
      <section id="corpsPage" class="col-sm-8">
        <div id="titrePage">
        <div  class="row"  >
            <article class="col-sm-4" id="formulaire">
              <form action="researchChill.php" method="post">
                <input class="champ" onfocus="if(this.value == this.defaultValue) this.value = ''" type="text" name="search" value="Search Chill..." id="researchBar" style="font-family: 'Rokkitt', serif;"/>
                    <input class="bouton" type="submit" value="search" style="font-family: 'Rokkitt', serif;" />
              </form>
            </article>
              <article class="col-sm-4" >
                <h2 > Result Chill Reasearch</h2>
              </article>
            </article>
            <article class="col-sm-1" style="font-size:20px;Line-Height: 30px; margin-top:3%;">
              Unconnect
              <a href="library/unco.php">
                <span class="glyphicon glyphicon-off"></span>
              </a></article>
           </div>
          
        </div>
        <div id="conteneurCat" class="row" >
          <article class="catSec" class="col-sm-3" style="overflow:auto;">
            <strong style="font-size:60px;">Result</strong><br>
              <br>
            <?php
                $found=false;
                foreach ($chill_cursor as $chill) {
                  
                  foreach($user->list_of_chill as $user_chill)
                  {
                    $e=preg_split("[\n]", $user_chill);
                    if($e[1]==$chill->location){
                        $found=true;
                    }
                  }
                  if($found==true){
                      $found=false;
                  }
                  else{
                  $e=preg_split("[,]", $chill->location);
                  $style="font-family: 'Lora', serif";
                  echo'
                        
                        <form action="library/addChill.php" method="post" style="Line-Height: 40px">
                          <input type="hidden" name="name" value="'.$chill->name.'">
                          <input type="hidden" name="adress" value="'.$e[0].'">
                          <input type="hidden" name="city" value="'.$e[1].'">
                          <input type="hidden" name="country" value="'.$e[2].'">
                          <input type="submit" value="'.$chill->name." ".$chill->location.'" class="chillList" style=" margin-left:6px; border-radius:6px;text-overflow: ellipsis;font-size: 20px;padding: 6px 0 2px 8px;'.$style.';">
                        </form>';
                      }

                }
            ?>
          </article>
        </div>
      </section>
    </div>
  </body>
</html>

