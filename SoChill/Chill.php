<?php
    session_cache_limiter('private_no_expire, must-revalidate');
    session_start();
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Chill - SoChill</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/common.css" rel="stylesheet">
    <link href="css/twoDivBis.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Mr+Dafoe|Rokkitt:400,500" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
     <script src="js/jquery.js"></script>
     <script type="text/javascript">
    
    var varia =0;

  function test(){
    var texte, doc;
    //texte="coucou";
    doc =document.getElementById("SelectChill").value;
    //alert(texte);
    texte=doc;
    alert(texte);
  }

  function link(lien){
    if(lien==1){
      window.location = "notif.html";
    }
    else if(lien==2){
       window.location = "message.html";
    }
    else if(lien==3){
       window.location = "parametre.html";
    }
  }


  function test2(){
    
    
    if(varia==0){
      $('#contenuSupp').append('<div onclick="link(1)" class="tetePageBisHerite">Notif</div><div onclick="link(2)" class="tetePageBisHerite">MSG</div><div onclick="link(3)" class="tetePageBisHerite">Parameter</div>');
      varia++;
    }
    else{
      $('.tetePageBisHerite').remove();
      varia--;
    }


  }
  
</script>

  <?php
    require "library/Class/user_managment_class.php";
    $name=$_POST["chill_place"];
    if(isset($_POST["chill_name"]) and isset($_POST["chill_place"])){

      $_SESSION["chill_name"]=$_POST["chill_name"];
      $_SESSION["chill_place"]=$_POST["chill_place"];
    }
    $e=$_POST["chill_name"];
    $mail=$_SESSION['mail'];
    $chill_n=chill_managment::research_chill_storage2($_SESSION["chill_name"],$_SESSION["chill_place"]);
    $chill=new chill($chill_n["name"],$chill_n["location"]);
    $chill->bsonUnserialize($chill_n);          
  ?>

  <script language="javascript" type="text/javascript">
         
         var i=0;
        
        function refreshTchat(){
        var chill_place= '<?php echo $_SESSION["chill_place"];?>';
        var chill_name='<?php echo $_SESSION["chill_name"];?>';
        
        $.ajax({ type: "GET", 
                  url: "ajax/refresh.php", 
                  data: "action=refresh&chill_place="+chill_place+"&chill_name="+chill_name, 
                  success: function(msg){ document.getElementById("divMessage").innerHTML = msg; } });
                  
          if(i==0){
          i++;
        }
        else{
          if(i==1)
          {
            $('#divMessage').scrollTop($('#divMessage')[0].scrollHeight);
            i++;
          }
         
      }
        setTimeout("refreshTchat()",400); 
        }



        function envoi(){

        var message = document.getElementById("boum").value;

        var pseudo = '<?php echo $_SESSION["mail"];?>';
        var chill_place= '<?php echo $_SESSION["chill_place"];?>';
        var chill_name='<?php echo $_SESSION["chill_name"];?>';

        $.ajax({ type: "GET",
                 url: "ajax/refresh.php", 
                 data: "action=envoi&message="+message+"&pseudo="+pseudo+"&chill_place="+chill_place+"&chill_name="+chill_name, 
                 success: function(msg){ document.getElementById("etat").innerHTML = msg;} });
        }

        function refresh2(){
          $('#divMessage').scrollTop($('#divMessage')[0].scrollHeight);
        }

        refreshTchat();

</script>
      
  </head>
  <body style="overflow:auto;">
	
    <div class="container" >
      

      <div class="row">
        



        <nav id="menu" class="col-sm-2">
          <div id="divLogo" class="row">
            <div id="tetePage" class="col-sm-12">
            <a href="./brick_panel/event/playlist.php" target="_blank" ><img id="logo" src="Image/frog_-_logo.png" alt="logo" height="140px" width="140px"/></a>
            </div>
          </div>
          <div class="menuCat" class="row">
            <div class="cat" class="col-lg-offset-2 col-sm-10">
              <div class="row" style="overflow:auto;Line-Height: 40px;">
              	
              	 	<strong>A Chill <br>is a place<br> where<br> you can<br>interact with<br>people inside<br> to chill out.<br> The<br> surf board<br> is a chat<br> to interact <br> with <br>people here</strong>
             	 
              </div>
              
            </div>
          </div>
          <div class="menuCat" class="row">
            <div class="cat" class="col-lg-offset-2 col-sm-10">
              <div class="row">
                <div class="Type">
                  <a href="index.php">Chill List</a>
               </div>
            </div>
          </div>
          
        </nav>
      
        <section id="corpsPage" class="col-sm-8">

            <div id="titrePage">
        <div  class="row"  >
            <article class="col-sm-4" id="formulaire">
              <form action="researchChill.php" method="post">
                <input class="champ" type="text" onfocus="if(this.value == this.defaultValue) this.value = ''" name="search" value="Search Chill..." id="researchBar" style="font-family: 'Rokkitt', serif;"/>
                    <input class="bouton" type="submit" value="search" style="font-family: 'Rokkitt', serif;" />
              </form>
              <a href="./library/unfollowChill.php" style="font-size:40px;font-family: 'Mr Dafoe'">
                Unfollow 
                  <span class="glyphicon glyphicon-remove" ></span>
              </a>
            </article>
            <article class="col-sm-4" >
              <h2 >Chill <?php echo $chill->name;?></h2>
            </article>
            
            <article class="col-sm-1" style="font-size:25px;Line-Height: 30px; margin-top:3%;">Unconnect
              <a href="library/unco.php">
                <span class="glyphicon glyphicon-off"></span>
              </a></article></article>
          </div>
          
        </div>
          
          <div id="conteneurCat" class="row">
            <article class="catSec" style="align:centre;">
              <strong style="font-size:60px;">Chill Vibes</strong> <br>
               <a href="brick_panel/addEventPage.php" class="btn btn-info btn-lg">
                  <span class="glyphicon glyphicon-plus"></span> Add Event 
              </a>
              <?php
              $index=count($chill->events)-1;
                while($index > -1){

                  $owner=user_managment::research_user2($chill->events[$index]->owner_mail);
                  echo'
                  <form action="brick_panel/event.php" method="post">
                        <input type="hidden" name="event_name" value="'.$chill->events[$index]->name.'">
                        <input type="hidden" name="event_date" value="'.$chill->events[$index]->aim->date.'">
                        <input type="submit" value="Name: '.$chill->events[$index]->name.' 
 
Owner: '.$owner->name.' '.$owner->surname.'" class="Type" style="background-color:rgba(242, 200, 150, 0.5);font-size:20px; Line-Height:30px;border-radius:20px; border-width:1px;">
                  </form>';
                  $index--;
                }
              ?>
              
          </div>
        </section>


         <nav id="message" style="font-family: 'Rokkitt', serif;" class="col-sm-2">
          <div class="row">

          </div>
          <div id="msgSec" class="row">
             <h1 id="titreMsg"><strong>Chill Trend</strong></h1>
            <div class="col-sm-12" id="plancheMessage">
              <form >
                <div id="SelectChill" name="nom" style="font-family: 'Mr Dafoe', cursive; font-size:30px;">
                  HEY!<br>
                  <div id="etat"></div>
                </div>
              </form>
                <div id="divMessage" style="border-style: double; border-radius:15px;overflow:auto;">
                  </div>
                <div id="inputMessage" style='margin-top:8%;' >
                  <input style="width:70%; height:30px;" onfocus="if(this.value == this.defaultValue) this.value = ''" onkeydown = "if (event.keyCode == 13)
                        document.getElementById('envoie').click()"  type="text" id ="boum" value="message ecrit"/>
                  <input id="envoie" type="submit" value ="Send" onclick="envoi(); setTimeout('refresh2()',499);"  style='margin-top:2%; width:30%; '>
                </div>
            </div>
          </div>
        </nav>
    </div>
  </body>
</html>






        
