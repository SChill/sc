<!DOCTYPE html>
<html>
  <head>
    <title>SoChill</title>
    <link href="SoChill/css/bootstrap.css" rel="stylesheet">
    <link href="SoChill/css/login.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Mr+Dafoe|Rokkitt:400,500" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="SoChill/js/jquery.js"></script>
    <script src="SoChill/js/nosFonctions.js"></script>
  </head>
  <body >
    <div class="container" >
    <div class="row">
      <nav id="menu" class="col-sm-2">
        <div class="row">
          <div id="tetePage" class="col-sm-4">
           <img id="logo" src="SoChill/Image/frog_-_logo.png" alt="logo" height="140px" width="140px"/>
          </div>
          
        </div>
        <div class="menuCat" class="row">
          <div class="cat" class="col-lg-offset-2 col-sm-10">
            <div class="row">
              <div id="Type" style="Line-Height:30px;">
                <strong>Let's add a place</strong><br><strong> to Chill out</strong>
              </div>
            </div>
          </div>
        </div>

        <div class="menuCat" class="row">
          <div class="cat" class="col-lg-offset-2 col-sm-10">
            <div class="row">
              <div id="Type" style="Line-Height:30px;">
                <strong>Founder:<br>Adrien Badré</strong><br><br><strong>Big Thanks to:<br> Axel Mascaro</strong>
              </div>
            </div>
          </div>
        </div>
      </nav>





      <section id="corpsPage" class="col-sm-8">
        <div id="titrePage">
        <div  class="row"  >
            
            <article class="col-sm-12" >
              <h2 >So Chill</h2>
            </article>
            
            
          </div>
          
        </div>
        <div id="conteneurCat" class="row">
          <article class="catSec" class="col-sm-3">
            <strong>Sign In !</strong><br/>
            <table id="signinTable">
            <form action="SoChill/library/adding_new_user.php" method="post">
              <tr>
                <td><strong>Name : </strong></td><td><input type="text" name="name" ></td>
              </tr>
              <tr>
                <td><strong>Surname : </strong></td><td><input type="text" name="surname" ></td>
              </tr>
              <tr>
                <td><strong>Email : </strong></td><td><input type="text" name="mail"> </td>
              </tr>
              <tr>
               <td><strong>Password : </strong></td><td> <input type="text" name="password"> </td>
               </tr>
              <tr>
               <td><strong>Confirm password : </strong></td><td><input type="text" name="Confirm_password"> </td>
               </tr>
               <tr> 
               <td><strong>Birth Day : </strong></td><td><input type="text" name="date_of_birth"> </td>
               </tr>
              <tr>
               <td><strong>Adress : </strong></td><td> <input type="text" name="adress"> </td>
              </tr>
              <tr>
               <td><strong>City : </strong></td><td> <input type="text" name="city"> </td>
              </tr>
              <tr>
               <td><strong>Country : </strong></td><td> <input type="text" name="country"> </td>
              </tr>
                <tr><td></td><td><input type="submit" value="sign-in"></td><td></td><td><strong>Have Fun Guys!</strong></td></tr>
             
            </form>
          </table> 
          </article>
        </div>
      </section>
      <nav id="message" class="col-sm-2">
        <div class="row">
        </div>
        <div id="msgSec" class="row">
          <h1 id="titreMsg">Login</h1>
          <div class="col-sm-12">
            <form id="formLogin" action="SoChill/library/connection.php" method="post">
              
                <strong>Mail : </strong><br/><input type="text" name="login" style="width:150px; margin-left:auto;margin-right:auto;" ><br/>
                <strong>Password : </strong><br/><input type="text" name="password" style="width:150px;"> <br/><br/><br/>
                <input type="submit" value="log-in">
             
            </form>
            <article class="col-sm-12">
            </article>
          </div>
        </div>
      </nav>
    </div>
  </body>
</html>
